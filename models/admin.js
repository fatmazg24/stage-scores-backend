const express = require("express");
const mongoose = require("mongoose");
const Admin = require("./models/adminmodel");
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/administrateurs", async (req, res) => {
    try {
        await Admin.find({})
        .then(result=>{
            res.send(result)
        });
        }
    catch (err) {
        console.log(err);
    }
});

app.post("/ajouter_administrateurs", async (req, res) => {
   try{
     let new_admin = new Admin({
       nom: req.body.nom,
       prenom: req.body.prenom,
       email: req.body.email,
       num_identite: req.body.num_identite,
       matricule_employé: req.body.matricule_employé,
       adresse: req.body.adresse,
       mdp: req.body.mdp,
     });
       await new_admin.save();
        res.send("save effectué avec succes!");
    } catch (err) {
        console.log(err);
    }
});

mongoose.connect('mongodb+srv://fatmazg:fatmazg@cluster0.wuzkfd1.mongodb.net/database?retryWrites=true&w=majority&appName=Cluster0', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Connexion à la base de données MongoDB réussie');
}).catch((err) => {
    console.error('Erreur de connexion à la base de données MongoDB :', err);
});



app.listen(5000,()=> console.log("serveur en marche"));
